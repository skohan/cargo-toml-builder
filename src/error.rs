use std::{
    fmt,
    error::Error as StdError,
};

/// Represents various errors that can occur
#[derive(Debug, Clone, PartialEq)]
pub struct Error {
    inner: ErrorKind,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        StdError::description(&self.inner)
    }
}

impl From<String> for Error {
    fn from(s: String) -> Error {
        Error { inner: From::from(s) }
    }
}

impl<'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Error {
        Error { inner: From::from(s) }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum ErrorKind {
    Custom(String),
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            ErrorKind::Custom(ref s) => f.write_str(s),
        }
    }
}

impl StdError for ErrorKind {
    fn description(&self) -> &str {
        match *self {
            ErrorKind::Custom(ref s) => s,
        }
    }
}

impl From<String> for ErrorKind {
    fn from(s: String) -> ErrorKind {
        ErrorKind::Custom(s)
    }
}

impl<'a> From<&'a str> for ErrorKind {
    fn from(s: &'a str) -> ErrorKind {
        ErrorKind::Custom(s.to_string())
    }
}

