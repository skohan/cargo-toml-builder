use std::default::Default;

use crate::types::Dependency;

/// Represents a single feature
#[derive(Default, Debug, Clone, PartialEq)]
pub struct Feature {
    pub(crate) label: String,
    pub(crate) deps: Vec<Dependency>,
    pub(crate) features: Vec<String>,
}

impl Feature {
    /// Constructs a new, empty feature from it's label
    pub fn new(label: &str) -> Feature {
        let mut f = Feature::default();
        f.label = label.to_string();
        f
    }

    /// Adds a dependency to the list for this feature
    pub fn dependency<D: Into<Dependency>>(&mut self, dep: D) -> &mut Self {
        self.deps.push(dep.into());
        self
    }

    /// Sets the list of dependencies for this feature
    pub fn dependencies<D: Into<Dependency>>(&mut self, deps: Vec<D>) -> &mut Self {
        self.deps = deps.into_iter().map(|d| d.into()).collect();
        self
    }

    /// Adds a feature to the list
    pub fn feature(&mut self, feature: &str) -> &mut Self {
        self.features.push(feature.to_string());
        self
    }

    /// Sets the list of features
    ///
    /// *WILL* replace any existing features
    pub fn features(&mut self, features: &[String]) -> &mut Self {
        self.features = features.to_vec();
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Self {
        self.clone()
    }
}

impl<'a> From<&'a mut Feature> for Feature {
    fn from(f: &'a mut Feature) -> Feature {
        f.clone()
    }
}

impl<'a> From<&'a str> for Feature {
    fn from(s: &'a str) -> Feature {
        Feature {
            label: s.to_string(),
            deps: vec![],
            features: vec![],
        }
    }
}

