use std::collections::BTreeMap;

pub use self::badges::*;
pub use self::dependency::*;
pub use self::feature::*;
pub use self::license::*;
pub use self::profile::*;
pub use self::target::*;

mod badges;
mod dependency;
mod feature;
mod license;
mod profile;
mod target;

#[derive(Debug, Clone, PartialEq, Default)]
/// Represents the `[workspace]` section of a Cargo.toml file
pub struct Workspace {
    pub(crate) members: Option<Vec<String>>,
    pub(crate) exclude: Option<Vec<String>>,
}

impl Workspace {
    /// Constructs a new, empty `Workspace`
    pub fn new() -> Workspace {
        Default::default()
    }

    /// Adds a single `member` to this builder
    ///
    /// Will _not_ affect any existing members that have been added
    pub fn member(&mut self, member: &str) -> &mut Self {
        if let Some(ref mut v) = self.members {
            v.push(member.into());
        } else {
            self.members = Some(vec![member.into()]);
        }
        self
    }

    /// Adds a set of `members` to this builder
    ///
    /// *WILL* replace any existing members that have been added
    pub fn members(&mut self, members: &[String]) -> &mut Self {
        self.members = Some(members.to_vec());
        self
    }

    /// Adds an `exclude` to this builder
    ///
    /// Will _not_ affect any other excludes that have been added
    pub fn exclude(&mut self, exclude: &str) -> &mut Self {
        if let Some(ref mut v) = self.exclude {
            v.push(exclude.into());
        } else {
            self.exclude = Some(vec![exclude.into()]);
        }
        self
    }

    /// Adds a set of `excludes` to this builder
    ///
    /// *WILL* replace any existing excludes that have been added
    pub fn excludes(&mut self, excludes: &[String]) -> &mut Self {
        self.exclude = Some(excludes.to_vec());
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Self {
        self.clone()
    }
}


/// Represents a patchset for a Cargo.toml
#[derive(Debug, Clone, PartialEq)]
pub struct PatchSet {
    pub(crate) label: String,
    pub(crate) patches: Vec<Dependency>,
}

impl PatchSet {
    /// Constructs a new, empty patchset
    pub fn new(label: &str) -> PatchSet {
        PatchSet {
            label: label.into(),
            patches: vec![],
        }
    }

    /// Adds a single patch to this builder
    ///
    /// Will _not_ affect any other patches that have been added
    pub fn patch<D: Into<Dependency>>(&mut self, patch: D) -> &mut Self {
        self.patches.push(patch.into());
        self
    }

    /// Adds a set of patches to this builder
    ///
    /// *WILL* replace any existing patches that have been added to this builder
    pub fn patches(&mut self, patches: &[Dependency]) -> &mut Self {
        self.patches = patches.to_vec();
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Self {
        self.clone()
    }
}

/// Represents a dependency replacement for a Cargo.toml
#[derive(Debug, Clone, PartialEq)]
pub struct Replace(pub(crate) Dependency);

impl Replace {
    /// Creates a new entry in the [replace] section of a Cargo.toml
    pub fn new<D: Into<Dependency>>(dep: D) -> Replace {
        Replace(dep.into())
    }
}

/// Represents the metadata table for a Cargo.toml
#[derive(Debug, Clone, PartialEq)]
pub struct MetadataTable {
    pub(crate) label: String,
    pub(crate) data: BTreeMap<String, String>,
}
