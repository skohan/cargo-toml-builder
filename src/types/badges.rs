use std::fmt;
use std::convert::TryFrom;

use crate::error::Error;

/// Represents an `appveyor = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct Appveyor {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
    pub(crate) service: Option<AppveyorService>,
}

/// Represents the values that `service` can take in an `appveyor = { ... }` badge
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AppveyorService {
    /// `appveyor = { service = "github" }`
    Github,
    /// `appveyor = { service = "bitbucket" }`
    Bitbucket,
    /// `appveyor = { service = "gitlab" }`
    Gitlab,
}

impl fmt::Display for AppveyorService {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use crate::types::badges::AppveyorService::*;
        write!(f, "{}", match *self {
            Github => "github",
            Bitbucket => "bitbucket",
            Gitlab => "gitlab",
        })
    }
}

impl From<String> for Appveyor {
    fn from(s: String) -> Appveyor {
        Appveyor::new(&s)
    }
}

impl<'a> From<&'a str> for Appveyor {
    fn from(s: &'a str) -> Appveyor {
        Appveyor::new(s)
    }
}

impl Appveyor {
    /// Creates a new, empty `appveyor` section
    pub fn new(repository: &str) -> Appveyor {
        Appveyor {
            repository: repository.to_string(),
            branch: None,
            service: None,
        }
    }

    /// Sets the branch this badge should be querying for
    pub fn branch(&mut self, branch: &str) -> &mut Self {
        self.branch = Some(branch.to_string());
        self
    }

    /// Sets the appveyor service this badge should be querying
    pub fn service(&mut self, service: AppveyorService) -> &mut Self {
        self.service = Some(service);
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Self {
        self.clone()
    }
}

/// Represents a `circe-ci = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct CircleCi {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
}

impl From<String> for CircleCi {
    fn from(s: String) -> CircleCi {
        CircleCi::new(&s)
    }
}

impl<'a> From<&'a str> for CircleCi {
    fn from(s: &'a str) -> CircleCi {
        CircleCi::new(s)
    }
}

impl CircleCi {
    /// Creates a new, empty `circle-ci` section
    pub fn new(repository: &str) -> CircleCi {
        CircleCi {
            repository: repository.to_string(),
            branch: None,
        }
    }

    /// Sets the branch this badge should be querying for
    pub fn branch(&mut self, branch: &str) -> &mut Self {
        self.branch = Some(branch.to_string());
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Self {
        self.clone()
    }
}

/// Represents a `gitlab = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct Gitlab {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
}

impl From<String> for Gitlab {
    fn from(s: String) -> Gitlab {
        Gitlab::new(&s)
    }
}

impl<'a> From<&'a str> for Gitlab {
    fn from(s: &'a str) -> Gitlab {
        Gitlab::new(s)
    }
}

impl Gitlab {
    /// Creates a new empty `gitlab` section
    pub fn new(repository: &str) -> Gitlab {
        Gitlab {
            repository: repository.to_string(),
            branch: None,
        }
    }

    /// Sets the branch this badge should retrieve information for
    pub fn branch(&mut self, branch: &str) -> &mut Gitlab {
        self.branch = Some(branch.to_string());
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Gitlab {
        self.clone()
    }
}

/// Represents a `travis-ci = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct TravisCi {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
}

impl From<String> for TravisCi {
    fn from(s: String) -> TravisCi {
        TravisCi::new(&s)
    }
}

impl<'a> From<&'a str> for TravisCi {
    fn from(s: &'a str) -> TravisCi {
        TravisCi::new(s)
    }
}

impl TravisCi {
    /// Creates a new `travis-ci` section
    pub fn new(repository: &str) -> TravisCi {
        TravisCi {
            repository: repository.to_string(),
            branch: None,
        }
    }

    /// Sets the branch that travis should build
    pub fn branch(&mut self, branch: &str) -> &mut TravisCi {
        self.branch = Some(branch.to_string());
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> TravisCi {
        self.clone()
    }
}

/// Represents a `codecov = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct Codecov {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
    pub(crate) service: Option<CodecovService>,
}

impl From<String> for Codecov {
    fn from(s: String) -> Codecov {
        Codecov::new(&s)
    }
}

impl<'a> From<&'a str> for Codecov {
    fn from(s: &'a str) -> Codecov {
        Codecov::new(s)
    }
}

/// Represents the values `service` can take in a `codecov = { ... }` badge
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum CodecovService {
    /// `codecov = { service = "github" }`
    Github,
    /// `codecov = { service = "bitbucket" }`
    Bitbucket,
    /// `codecov = { service = "gitlab" }`
    Gitlab,
}

impl fmt::Display for CodecovService {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use crate::types::badges::CodecovService::*;
        write!(f, "{}", match *self {
            Github => "github",
            Bitbucket => "bitbucket",
            Gitlab => "gitlab",
        })
    }
}

impl Codecov {
    /// Creates a new empty `codecov` section
    pub fn new(repository: &str) -> Codecov {
        Codecov {
            repository: repository.to_string(),
            branch: None,
            service: None,
        }
    }

    /// Sets the branch that codecov should inspect
    pub fn branch(&mut self, branch: &str) -> &mut Codecov {
        self.branch = Some(branch.to_string());
        self
    }

    /// Sets the service that codecov will be interacting with
    pub fn service(&mut self, service: CodecovService) -> &mut Codecov {
        self.service = Some(service);
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Codecov {
        self.clone()
    }
}

/// Represents a `coveralls = { ... }` badge
#[derive(Debug, Clone, PartialEq)]
pub struct Coveralls {
    pub(crate) repository: String,
    pub(crate) branch: Option<String>,
    pub(crate) service: Option<CoverallsService>,
}

/// Represents the values `service` can take in a `coveralls = { ... }` badge
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum CoverallsService {
    /// `coveralls = { service = "github" }`
    Github,
    /// `coveralls = { service = "bitbucket" }`
    Bitbucket,
    /// `coveralls = { service = "gitlab" }`
    Gitlab,
}

impl fmt::Display for CoverallsService {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use crate::types::badges::CoverallsService::*;
        write!(f, "{}", match *self {
            Github => "github",
            Bitbucket => "bitbucket",
            Gitlab => "gitlab",
        })
    }
}

impl From<String> for Coveralls {
    fn from(s: String) -> Coveralls {
        Coveralls::new(&s)
    }
}

impl<'a> From<&'a str> for Coveralls {
    fn from(s: &'a str) -> Coveralls {
        Coveralls::new(s)
    }
}

impl Coveralls {
    /// Creates a new `coveralls = {}` section
    pub fn new(repository: &str) -> Coveralls {
        Coveralls {
            repository: repository.to_string(),
            branch: None,
            service: None,
        }
    }

    /// Sets the branch that coveralls should inspect
    pub fn branch(&mut self, branch: &str) -> &mut Coveralls {
        self.branch = Some(branch.to_string());
        self
    }

    /// Sets the coveralls service value
    pub fn service(&mut self, service: CoverallsService) -> &mut Coveralls {
        self.service = Some(service);
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Coveralls {
        self.clone()
    }
}

/// Represents a `maintenance = { status = ... }` badge
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Maintenance {
    pub(crate) status: MaintenanceStatus,
}

impl Maintenance {
    fn new(status: MaintenanceStatus) -> Maintenance {
        Maintenance { status: status }
    }
}

impl TryFrom<String> for Maintenance {
    type Error = Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        Ok(Maintenance::new(MaintenanceStatus::try_from(s)?))
    }
}

impl<'a> TryFrom<&'a str> for Maintenance {
    type Error = Error;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        Ok(Maintenance::new(MaintenanceStatus::try_from(s)?))
    }
}

impl From<MaintenanceStatus> for Maintenance {
    fn from(m: MaintenanceStatus) -> Maintenance {
        Maintenance {
            status: m,
        }
    }
}

/// Represents the values `status` can take in a `maintenance = { ... }` badge
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum MaintenanceStatus {
    /// `maintenance = { status = "actively-developed" }`
    Actively,
    /// `maintenance = { status = "passively-maintained" }`
    Passively,
    /// `maintenance = { status = "as-is" }`
    AsIs,
    /// `maintenance = { status = "none" }`
    None,
    /// `maintenance = { status = "experimental" }`
    Experimental,
    /// `maintenance = { status = "looking-for-maintainer" }`
    LookingForMaintainer,
    /// `maintenance = { status = "deprecated" }`
    Deprecated,
}

impl TryFrom<String> for MaintenanceStatus {
    type Error = Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        TryFrom::try_from(s.as_str())
    }
}
impl<'a> TryFrom<&'a str> for MaintenanceStatus {
    type Error = Error;
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        Ok(match s {
            "actively-developed" => MaintenanceStatus::Actively,
            "passively-maintained" => MaintenanceStatus::Passively,
            "as-is" => MaintenanceStatus::AsIs,
            "none" => MaintenanceStatus::None,
            "experimental" => MaintenanceStatus::Experimental,
            "looking-for-maintainer" => MaintenanceStatus::LookingForMaintainer,
            "deprecated" => MaintenanceStatus::Deprecated,
            _ => return Err("unrecognized maintenance status string".into()),
        })
    }
}

impl fmt::Display for MaintenanceStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match *self {
            MaintenanceStatus::Actively => "actively-developed",
            MaintenanceStatus::Passively => "passively-maintained",
            MaintenanceStatus::AsIs => "as-is",
            MaintenanceStatus::None => "none",
            MaintenanceStatus::Experimental => "experimental",
            MaintenanceStatus::LookingForMaintainer => "looking-for-maintainer",
            MaintenanceStatus::Deprecated => "deprecated",
        })
    }
}
