use std::fmt;

/// Licenses for the `license = ""` attribute
#[derive(Debug, Clone, PartialEq)]
pub enum License {
    /// `license = "MIT"`
    Mit,
    /// `license = "APACHE-2.0"`
    Apache2,
    /// `license = "GPL-3.0"`
    Gplv3,
    /// `license = "BSD"`
    Bsd,
    /// `license = "MPL"`
    Mpl,
    /// `license = "AGPL-3.0"`
    Agplv3,
    /// Any other license
    Other(String),
}

impl From<String> for License {
    fn from(s: String) -> License {
        License::from(s.as_str())
    }
}

impl<'a> From<&'a str> for License {
    fn from(s: &'a str) -> License {
        match s.to_lowercase().as_str() {
            "mit" => License::Mit,
            "apache2" => License::Apache2,
            "apache-2.0" => License::Apache2,
            "gpl" => License::Gplv3,
            "gplv3" => License::Gplv3,
            "gpl-3.0" => License::Gplv3,
            "bsd" => License::Bsd,
            "mpl" => License::Mpl,
            "agpl" => License::Agplv3,
            "agplv3" => License::Agplv3,
            "agpl-3.0" => License::Agplv3,
            _ => License::Other(s.to_string()),
        }
    }
}

impl fmt::Display for License {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            &License::Mit => "MIT",
            &License::Apache2 => "Apache-2.0",
            &License::Gplv3 => "GPL-3.0",
            &License::Bsd => "BSD",
            &License::Mpl => "MPL",
            &License::Agplv3 => "AGPL-3.0",
            &License::Other(ref s) => s,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_string() {
        assert_eq!(License::from("mit"), License::Mit);
        assert_eq!(License::from("MIT"), License::Mit);
        assert_eq!(License::from("apache2"), License::Apache2);
        assert_eq!(License::from("Apache2"), License::Apache2);
        assert_eq!(License::from("Apache-2.0"), License::Apache2);
        assert_eq!(License::from("GPL"), License::Gplv3);
        assert_eq!(License::from("gpl"), License::Gplv3);
        assert_eq!(License::from("GPL-3.0"), License::Gplv3);
        assert_eq!(License::from("GPLv3"), License::Gplv3);
        assert_eq!(License::from("gplv3"), License::Gplv3);
        assert_eq!(License::from("bsd"), License::Bsd);
        assert_eq!(License::from("BSD"), License::Bsd);
        assert_eq!(License::from("mpl"), License::Mpl);
        assert_eq!(License::from("MPL"), License::Mpl);
        assert_eq!(License::from("agpl"), License::Agplv3);
        assert_eq!(License::from("AGPL"), License::Agplv3);
        assert_eq!(License::from("agplv3"), License::Agplv3);
        assert_eq!(License::from("AGPLv3"), License::Agplv3);
        assert_eq!(License::from("AGPL-3.0"), License::Agplv3);
        assert_eq!(License::from("CUSTOM"), License::Other("CUSTOM".to_string()));
    }

    #[test]
    fn test_to_string() {
        assert_eq!(License::Mit.to_string().as_str(), "MIT");
        assert_eq!(License::Apache2.to_string().as_str(), "Apache-2.0");
        assert_eq!(License::Gplv3.to_string().as_str(), "GPL-3.0");
        assert_eq!(License::Bsd.to_string().as_str(), "BSD");
        assert_eq!(License::Mpl.to_string().as_str(), "MPL");
        assert_eq!(License::Agplv3.to_string().as_str(), "AGPL-3.0");
        assert_eq!(License::Other("CUSTOM".to_string()).to_string().as_str(), "CUSTOM");
    }
}
