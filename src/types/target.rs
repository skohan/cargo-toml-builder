use std::fmt;
use std::default::Default;
use std::convert::TryFrom;

use crate::error::Error;

/// Represents a `[lib]` table
#[derive(Debug, Default, Clone, PartialEq)]
pub struct LibTarget {
    common: CommonTarget,
    crate_type: Option<CrateType>,
}

/// Builder for [LibTarget](../types/struct.LibTarget.html) types
#[derive(Debug, Default, Clone, PartialEq)]
pub struct LibTargetBuilder {
    name: Option<String>,
    path: Option<String>,
    test: Option<bool>,
    doctest: Option<bool>,
    bench: Option<bool>,
    doc: Option<bool>,
    plugin: Option<bool>,
    proc_macro: Option<bool>,
    harness: Option<bool>,
    crate_type: Option<CrateType>,
}

impl LibTarget {
    /// Constructs a new builder for a lib target
    pub fn new() -> LibTargetBuilder {
        Default::default()
    }

    /// Returns the value of the name of this lib target
    pub fn name(&self) ->  Option<&String> {
        self.common.name.as_ref()
    }

    /// Returns the value of the path to the root for this lib target
    pub fn path(&self) ->  Option<&String> {
        self.common.path.as_ref()
    }

    /// Returns the value of the test flag for this lib target
    pub fn test(&self) ->  Option<bool> {
        self.common.test
    }

    /// Returns the value of the doctest flag for this lib target
    pub fn doctest(&self) ->  Option<bool> {
        self.common.doctest
    }

    /// Returns the value of the bench flag for this lib target
    pub fn bench(&self) ->  Option<bool> {
        self.common.bench
    }

    /// Returns the value of the doc flag for this lib target
    pub fn doc(&self) ->  Option<bool> {
        self.common.doc
    }

    /// Returns the value of the plugin flag for this lib target
    pub fn plugin(&self) ->  Option<bool> {
        self.common.plugin
    }

    /// Returns the value of the proc-macro flag for this lib target
    pub fn proc_macro(&self) ->  Option<bool> {
        self.common.proc_macro
    }

    /// Returns the value of the harness flag for this lib target
    pub fn harness(&self) ->  Option<bool> {
        self.common.harness
    }

    /// Returns the value of the crate-type for this lib target
    pub fn crate_type(&self) ->  Option<CrateType> {
        self.crate_type
    }

}

impl LibTargetBuilder {
    /// Sets the name of this lib target
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.name = Some(name.into());
        self
    }

    /// Sets the path to the root for this lib target
    pub fn path(&mut self, path: &str) -> &mut Self {
        self.path = Some(path.into());
        self
    }

    /// Sets the test flag for this lib target
    pub fn test(&mut self, test: bool) -> &mut Self {
        self.test = Some(test);
        self
    }

    /// Sets the doctest flag for this lib target
    pub fn doctest(&mut self, doctest: bool) -> &mut Self {
        self.doctest = Some(doctest);
        self
    }

    /// Sets the bench flag for this lib target
    pub fn bench(&mut self, bench: bool) -> &mut Self {
        self.bench = Some(bench);
        self
    }

    /// Sets the doc flag for this lib target
    pub fn doc(&mut self, doc: bool) -> &mut Self {
        self.doc = Some(doc);
        self
    }

    /// Sets the plugin flag for this lib target
    pub fn plugin(&mut self, plugin: bool) -> &mut Self {
        self.plugin = Some(plugin);
        self
    }

    /// Sets the proc-macro flag for this lib target
    pub fn proc_macro(&mut self, proc_macro: bool) -> &mut Self {
        self.proc_macro = Some(proc_macro);
        self
    }

    /// Sets the harness flag for this lib target
    pub fn harness(&mut self, harness: bool) -> &mut Self {
        self.harness = Some(harness);
        self
    }

    /// Sets the crate-type for this lib target
    pub fn crate_type(&mut self, crate_type: CrateType) -> &mut Self {
        self.crate_type = Some(crate_type);
        self
    }

    /// Constructs an instance of `LibTarget` from this builder
    pub fn build(&self) -> LibTarget {
        let common = CommonTarget {
            name: self.name.clone(),
            path: self.path.clone(),
            test: self.test.clone(),
            doctest: self.doctest.clone(),
            bench: self.bench.clone(),
            doc: self.doc.clone(),
            plugin: self.plugin.clone(),
            proc_macro: self.proc_macro.clone(),
            harness: self.harness.clone(),
        };
        LibTarget {
            common: common,
            crate_type: self.crate_type.clone(),
        }
    }
}

impl<'a> From<&'a mut LibTargetBuilder> for LibTarget {
    fn from(target: &'a mut LibTargetBuilder) -> LibTarget {
        target.build()
    }
}

#[derive(Debug, Default, Clone, PartialEq)]
struct CommonTarget {
    name: Option<String>,
    path: Option<String>,
    test: Option<bool>,
    doctest: Option<bool>,
    bench: Option<bool>,
    doc: Option<bool>,
    plugin: Option<bool>,
    proc_macro: Option<bool>,
    harness: Option<bool>,
}

/// Represents `[[bench]]`, `[[bin]]`, `[[example]]`, and `[[test]]` tables
#[derive(Debug, Default, Clone, PartialEq)]
pub struct NonLibTarget {
    common: CommonTarget,
    required_features: Option<Vec<String>>,
}

/// Builder for [NonLibTarget](../types/struct.NonLibTarget.html) types
#[derive(Debug, Default, Clone, PartialEq)]
pub struct NonLibTargetBuilder {
    name: Option<String>,
    path: Option<String>,
    test: Option<bool>,
    doctest: Option<bool>,
    bench: Option<bool>,
    doc: Option<bool>,
    plugin: Option<bool>,
    proc_macro: Option<bool>,
    harness: Option<bool>,
    required_features: Option<Vec<String>>,
}

impl NonLibTarget {
    /// Constructs a builder for a non-lib target (bin, example, bench, test)
    pub fn new() -> NonLibTargetBuilder {
        Default::default()
    }

    /// Returns the value of the name of this target
    pub fn name(&self) -> Option<&String> {
        self.common.name.as_ref()
    }

    /// Returns the value of the path to the root for this target
    pub fn path(&self) -> Option<&String> {
        self.common.path.as_ref()
    }

    /// Returns the value of the test flag for this target
    pub fn test(&self) -> Option<bool> {
        self.common.test
    }

    /// Returns the value of the doctest flag for this target
    pub fn doctest(&self) -> Option<bool> {
        self.common.doctest
    }

    /// Returns the value of the bench flag for this target
    pub fn bench(&self) -> Option<bool> {
        self.common.bench
    }

    /// Returns the value of the doc flag for this target
    pub fn doc(&self) -> Option<bool> {
        self.common.doc
    }

    /// Returns the value of the plugin flag for this target
    pub fn plugin(&self) ->  Option<bool> {
        self.common.plugin
    }

    /// Returns the value of the proc-macro flag for this target
    pub fn proc_macro(&self) -> Option<bool> {
        self.common.proc_macro
    }

    /// Returns the value of the harness flag for this target
    pub fn harness(&self) -> Option<bool> {
        self.common.harness
    }

    /// Returns the value of the required-features value for this target
    pub fn required_features(&self) -> Option<&Vec<String>> {
        self.required_features.as_ref()
    }
}

impl NonLibTargetBuilder {
    /// Sets the name of this target
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.name = Some(name.into());
        self
    }

    /// Sets the path to the root for this target
    pub fn path(&mut self, path: &str) -> &mut Self {
        self.path = Some(path.into());
        self
    }

    /// Sets the test flag for this target
    pub fn test(&mut self, test: bool) -> &mut Self {
        self.test = Some(test);
        self
    }

    /// sets the doctest flag for this target
    pub fn doctest(&mut self, doctest: bool) -> &mut Self {
        self.doctest = Some(doctest);
        self
    }

    /// sets the bench flag for this target
    pub fn bench(&mut self, bench: bool) -> &mut Self {
        self.bench = Some(bench);
        self
    }

    /// sets the doc flag for this target
    pub fn doc(&mut self, doc: bool) -> &mut Self {
        self.doc = Some(doc);
        self
    }

    /// Sets the plugin flag for this target
    pub fn plugin(&mut self, plugin: bool) -> &mut Self {
        self.plugin = Some(plugin);
        self
    }

    /// Sets the proc-macro flag for this target
    pub fn proc_macro(&mut self, proc_macro: bool) -> &mut Self {
        self.proc_macro = Some(proc_macro);
        self
    }

    /// sets the harness flag for this target
    pub fn harness(&mut self, harness: bool) -> &mut Self {
        self.harness = Some(harness);
        self
    }

    /// Sets a required feature for this target
    pub fn required_feature(&mut self, feature: &str) -> &mut Self {
        if let Some(ref mut v) = self.required_features {
            v.push(feature.into());
        } else {
            self.required_features = Some(vec![feature.into()]);
        }
        self
    }

    /// Constructs a `NonLibTarget` from this builder
    pub fn build(&self) -> NonLibTarget {
        let common = CommonTarget {
            name: self.name.clone(),
            path: self.path.clone(),
            test: self.test.clone(),
            doctest: self.doctest.clone(),
            bench: self.bench.clone(),
            doc: self.doc.clone(),
            plugin: self.plugin.clone(),
            proc_macro: self.proc_macro.clone(),
            harness: self.harness.clone(),
        };

        NonLibTarget {
            common: common,
            required_features: self.required_features.clone(),
        }
    }
}

impl<'a> From<&'a mut NonLibTargetBuilder> for NonLibTarget {
    fn from(t: &'a mut NonLibTargetBuilder) -> NonLibTarget {
        t.build()
    }
}

/// Represents the choices for the `crate-type` attribute
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum CrateType {
    /// `type = "dylib"`
    Dylib,
    /// `type = "rlib"`
    Rlib,
    /// `type = "staticlib"`
    Staticlib,
    /// `type = "cdylib"`
    Cdylib,
    /// `type = "proc-macro"`
    ProcMacro,
}

impl<'a> TryFrom<&'a str> for CrateType {
    type Error = Error;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        Ok(match s.to_lowercase().as_str() {
            "dylib" => CrateType::Dylib,
            "rlib" => CrateType::Rlib,
            "staticlib" => CrateType::Staticlib,
            "cdylib" => CrateType::Cdylib,
            "proc-macro" => CrateType::ProcMacro,
            _ => return Err("not a valid crate type".into()),
        })
    }
}

impl TryFrom<String> for CrateType {
    type Error = Error;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        TryFrom::try_from(s.as_str())
    }
}

impl fmt::Display for CrateType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match *self {
            CrateType::Dylib => "dylib",
            CrateType::Rlib => "rlib",
            CrateType::Staticlib => "staticlib",
            CrateType::Cdylib => "cdylib",
            CrateType::ProcMacro => "proc-macro",
        })
    }
}

/// Represents a `[[bin]]` table
/// 
/// See [NonLibTarget](../types/struct.NonLibTarget.html) docs for more
pub type BinTarget = NonLibTarget;

/// Represents a `[[bench]]` table
/// 
/// See [NonLibTarget](../types/struct.NonLibTarget.html) docs for more
pub type BenchTarget = NonLibTarget;

/// Represents a `[[test]]` table
/// 
/// See [NonLibTarget](../types/struct.NonLibTarget.html) docs for more
pub type TestTarget = NonLibTarget;

/// Represents an `[[example]]` table
/// 
/// See [NonLibTarget](../types/struct.NonLibTarget.html) docs for more
pub type ExampleTarget = NonLibTarget;

