use std::fmt;
use std::convert::TryFrom;

use crate::error::Error;

/// Represents a `[profile.*]` table
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct Profile {
    pub(crate) opt_level: Option<u8>,
    pub(crate) debug: Option<bool>,
    pub(crate) rpath: Option<bool>,
    pub(crate) lto: Option<bool>,
    pub(crate) debug_assertions: Option<bool>,
    pub(crate) codegen_units: Option<u64>,
    pub(crate) panic: Option<PanicStrategy>,
}

impl Profile {
    /// Constructs a new, empty profile
    pub fn new() -> Profile {
        Profile::default()
    }

    /// Sets the optimization level for this profile
    pub fn opt_level(&mut self, opt_level: u8) -> Result<&mut Self, Error> {
        if opt_level > 3 {
            return Err("invalid opt level".into());
        }
        self.opt_level = Some(opt_level);
        Ok(self)
    }

    /// Sets the debug flag for this profile
    pub fn debug(&mut self, debug: bool) -> &mut Self {
        self.debug = Some(debug);
        self
    }

    /// Sets the rpath flag for this profile
    pub fn rpath(&mut self, rpath: bool) -> &mut Self {
        self.rpath = Some(rpath);
        self
    }

    /// Sets the lto flag for this profile
    pub fn lto(&mut self, lto: bool) -> &mut Self {
        self.lto = Some(lto);
        self
    }

    /// Sets the debug-assertions flag for this profile
    pub fn debug_assertions(&mut self, debug_assertions: bool) -> &mut Self {
        self.debug_assertions = Some(debug_assertions);
        self
    }

    /// Sets the number of codegen units for this profile
    pub fn codegen_units(&mut self, codegen_units: u64) -> &mut Self {
        self.codegen_units = Some(codegen_units);
        self
    }

    /// Sets the panic strategy for this profile
    pub fn panic(&mut self, panic: PanicStrategy) -> &mut Self {
        self.panic = Some(panic);
        self
    }

    /// Takes ownership of this builder
    pub fn build(&self) -> Profile {
        self.clone()
    }
}

/// Represents the possible values for the `panic` setting
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PanicStrategy {
    /// `panic = "unwind"`
    Unwind,
    /// `panic = "abort"`
    Abort,
}

impl<'a> TryFrom<&'a str> for PanicStrategy {
    type Error = Error;
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        Ok(match s.to_lowercase().as_str() {
            "unwind" => PanicStrategy::Unwind,
            "abort" => PanicStrategy::Abort,
            s => return Err(format!("unknown panic strategy {}", s).into()),
        })
    }
}

impl TryFrom<String> for PanicStrategy {
    type Error = Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        TryFrom::try_from(s.as_str())
    }
}

impl fmt::Display for PanicStrategy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match *self {
            PanicStrategy::Unwind => "unwind",
            PanicStrategy::Abort => "abort",
        })
    }
}


