//! Cargo.toml Builder
//!
//! Programmatically create Cargo.toml files
//!
//! # Example
//!
//! ```rust
//! extern crate cargo_toml_builder;
//!
//! use cargo_toml_builder::prelude::*;
//! # use std::error::Error;
//! # fn main() -> Result<(), Box<dyn Error>> {
//! let cargo_toml = CargoToml::builder()
//!                     .name("my-project")
//!                     .version("1.0.0")
//!                     .author("Alice Smith <asmith@example.com>")
//!                     .dependency("env_logger".version("0.5.6"))
//!                     .build()?;
//! assert_eq!(cargo_toml.to_string(), r#"
//! [package]
//! name = "my-project"
//! version = "1.0.0"
//! authors = ["Alice Smith <asmith@example.com>"]
//!
//! [dependencies]
//! env_logger = "0.5.6"
//! "#);
//! #   Ok(())
//! # }
//! ```
#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications,
    rust_2018_compatibility,
    rust_2018_idioms
    )]

use std::collections::HashMap;
use std::default::Default;

use toml_edit::Document;

use crate::toml::TomlOutput;
use crate::types::*;
use crate::types::Dependency;

pub use crate::error::Error;

/// Includes all types used by the builder
pub mod types;
mod toml;
mod error;

/// Use the glob import from this module to pull in the most common traits & types needed to work
/// with the builder
pub mod prelude {
    pub use crate::CargoToml;
    pub use crate::types::{BinTarget, BenchTarget, Dependency, DependencyExt, ExampleTarget, Feature, LibTarget, License, Profile, TestTarget};
}

/// Main builder struct
///
/// This is the builder that all other methods are called on. You can use `::builder()`,
/// `::new()`, or `::default()` to get an initialized instance
///
/// # Example
///
/// ```
/// extern crate cargo_toml_builder;
/// use cargo_toml_builder::prelude::*;
/// # use std::error::Error;
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let cargo_toml = CargoToml::builder()
///                     .name("my-project")
///                     .author("Me <me@me.com>")
///                     .build()?;
///
/// assert_eq!(cargo_toml.to_string(), r#"
/// [package]
/// name = "my-project"
/// version = "0.1.0"
/// authors = ["Me <me@me.com>"]
///
/// [dependencies]
/// "#);
/// #   Ok(())
/// # }
/// ```
#[derive(Debug, Clone, PartialEq, Default)]
pub struct CargoToml {
    // [package]
    /// The name that will be used for this Cargo.toml
    pub name: Option<String>,
    /// The list of authors that will be used for this Cargo.toml
    pub authors: Option<Vec<String>>,
    version: Option<String>,
    build_script: Option<String>,
    documentation: Option<String>,
    exclude: Option<Vec<String>>,
    include: Option<Vec<String>>,
    publish: Option<bool>,
    workspace_root: Option<String>,
    description: Option<String>,
    homepage: Option<String>,
    repository: Option<String>,
    readme: Option<String>,
    keywords: Option<Vec<String>>,
    categories: Option<Vec<String>>,
    licenses: Option<Vec<License>>,
    license_file: Option<String>,
    edition: Option<String>,

    // [badges]
    appveyor: Option<Appveyor>,
    circle_ci: Option<CircleCi>,
    gitlab: Option<Gitlab>,
    travis_ci: Option<TravisCi>,
    codecov: Option<Codecov>,
    coveralls: Option<Coveralls>,
    is_it_maintained_time: Option<String>,
    is_it_maintained_issues: Option<String>,
    maintenance: Option<Maintenance>,

    // [package.metadata]
    metadata: Option<Vec<MetadataTable>>,

    // [dependencies]
    dependencies: Option<Vec<Dependency>>,

    // [dev-dependencies]
    dev_dependencies: Option<Vec<Dependency>>,

    // [build-dependencies]
    build_dependencies: Option<Vec<Dependency>>,

    // [target.*.dependencies]
    target_dependencies: Option<HashMap<String, Vec<Dependency>>>,

    // [profile.*]
    profile_dev: Option<Profile>,
    profile_release: Option<Profile>,
    profile_test: Option<Profile>,
    profile_bench: Option<Profile>,
    profile_doc: Option<Profile>,

    // [features]
    features: Option<Vec<Feature>>,

    // [workspace]
    workspace: Option<Workspace>,

    // [[bin]]
    bins: Option<Vec<BinTarget>>,

    // [lib]
    lib: Option<LibTarget>,

    // [[bench]]
    benches: Option<Vec<BenchTarget>>,

    // [[test]]
    tests: Option<Vec<TestTarget>>,

    // [[example]]
    examples: Option<Vec<ExampleTarget>>,

    // [patch.*]
    patches: Option<Vec<PatchSet>>,

    // [replace]
    replacements: Option<Vec<Replace>>,
}

impl CargoToml {
    /// Construct a new instance
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::CargoToml;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new();
    /// #   Ok(())
    /// # }
    /// ```
    pub fn new() -> CargoToml {
        Default::default()
    }

    /// Construct a new instance
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::CargoToml;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::builder();
    /// #   Ok(())
    /// # }
    /// ```
    pub fn builder() -> CargoToml {
        Default::default()
    }

    /// Sets the `package.name` of the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::CargoToml;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .name("my-rust-project")
    /// #                   .author("me")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"name = "my-rust-project""#));
    /// /*
    /// [package]
    /// name = "my-rust-project"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.name = Some(name.to_string());
        self
    }

    /// Add an author to the `package.authors` property
    ///
    /// Note: This will append an entry to the underlying Vec, it will not affect any other
    /// `author`s that have been set
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .author("Me <me@me.com>")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"authors = ["Me <me@me.com>"]"#));
    /// /*
    /// [package]
    /// authors = ["Me <me@me.com>"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn author(&mut self, author: &str) -> &mut Self {
        if let Some(ref mut v) = self.authors {
            v.push(author.to_string());
        } else {
            self.authors = Some(vec![author.to_string()]);
        }
        self
    }

    /// Set the list of authors for the `package.authors` property
    ///
    /// Note: This will _replace_ any existing list of authors
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .authors(&[
    ///                         "Me <me@me.com>".into(),
    ///                         "You <you@you.com>".into()
    ///                     ])
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"authors = ["Me <me@me.com>", "You <you@you.com>"]"#));
    /// /*
    /// [package]
    /// authors = ["Me <me@me.com>", "You <you@you.com>"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn authors(&mut self, authors: &[String]) -> &mut Self {
        self.authors = Some(authors.to_vec());
        self
    }

    /// Sets the `package.version` of the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                         .version("1.1.0")
    /// #                       .author("me")
    /// #                       .name("my-project")
    ///                         .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"version = "1.1.0""#));
    /// /*
    /// [package]
    /// version = "1.1.0"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn version(&mut self, version: &str) -> &mut Self {
        self.version = Some(version.into());
        self
    }

    /// Sets the `package.build` property of the project
    ///
    /// TODO: currently impossible to generate `build = false`
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .build_script("./my-build-script.rs")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"build = "./my-build-script.rs""#));
    /// /*
    /// [package]
    /// build = "./my-build-script"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn build_script(&mut self, build: &str) -> &mut Self {
        self.build_script = Some(build.into());
        self
    }

    /// Sets the `package.documentation` property of the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .documentation("https://docs.rs/my-project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"documentation = "https://docs.rs/my-project""#));
    /// /*
    /// [package]
    /// documentation = "https://docs.rs/my-project"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn documentation(&mut self, documentation: &str) -> &mut Self {
        self.documentation = Some(documentation.into());
        self
    }

    /// Adds an exclusion pattern to the `package.exclude` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .exclude("build/**/*.o")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"exclude = ["build/**/*.o"]"#));
    /// /*
    /// [package]
    /// exclude = ["build/**/*.o"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn exclude(&mut self, exclude: &str) -> &mut Self {
        if let Some(ref mut v) = self.exclude {
            v.push(exclude.into());
        } else {
            self.exclude = Some(vec![exclude.into()]);
        }
        self
    }

    /// Sets the list of exclusion patterns for the `package.exclude` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .excludes(&[
    ///                         "build/**/*.o".into(),
    ///                         "doc/**/*.html".into(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"exclude = ["build/**/*.o", "doc/**/*.html"]"#));
    /// /*
    /// [package]
    /// exclude = ["build/**/*.o", "doc/**/*.html"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn excludes(&mut self, excludes: &[String]) -> &mut Self {
        self.exclude = Some(excludes.to_vec());
        self
    }

    /// Adds an inclusion pattern to the `package.include` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .include("src/**/*")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"include = ["src/**/*"]"#));
    /// /*
    /// [package]
    /// include = ["src/**/*"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn include(&mut self, include: &str) -> &mut Self {
        if let Some(ref mut v) = self.include {
            v.push(include.into());
        } else {
            self.include = Some(vec![include.into()]);
        }
        self
    }

    /// Sets the inclusion patterns for the `package.include` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .includes(&[
    ///                         "src/**/*".into(),
    ///                         "Cargo.toml".into(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"include = ["src/**/*", "Cargo.toml"]"#));
    /// /*
    /// [package]
    /// include = ["src/**/*.html, "Cargo.toml"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn includes(&mut self, includes: &[String]) -> &mut Self {
        self.include = Some(includes.to_vec());
        self
    }

    /// Sets the `package.publish` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .publish(false)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"publish = false"#));
    /// /*
    /// [package]
    /// publish = false
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn publish(&mut self, publish: bool) -> &mut Self {
        self.publish = Some(publish);
        self
    }

    /// Sets the `package.workspace` property
    ///
    /// (this is the `workspace = "/path/to/workspace"` property, not be be confused with the
    /// `[workspace]` table that can also be added to the document)
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .workspace_root("/path/to/workspace/root")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"workspace = "/path/to/workspace/root""#));
    /// /*
    /// [package]
    /// workspace = "/path/to/workspace/root"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn workspace_root(&mut self, workspace_root: &str) -> &mut Self {
        self.workspace_root = Some(workspace_root.into());
        self
    }

    /// Sets the `package.description` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .description("This is my new Rust project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"description = "This is my new Rust project""#));
    /// /*
    /// [package]
    /// description = "This is my new Rust project"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn description(&mut self, description: &str) -> &mut Self {
        self.description = Some(description.into());
        self
    }

    /// Sets the `package.homepage` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .homepage("https://my.domain/")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"homepage = "https://my.domain/""#));
    /// /*
    /// [package]
    /// homepage = "https://my.domain/"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn homepage(&mut self, homepage: &str) -> &mut Self {
        self.homepage = Some(homepage.into());
        self
    }

    /// Sets the `package.repository` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .repository("https://gitlab.com/me/my-project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"repository = "https://gitlab.com/me/my-project""#));
    /// /*
    /// [package]
    /// repository = "https://gitlab.com/me/my-project"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn repository(&mut self, repository: &str) -> &mut Self {
        self.repository = Some(repository.into());
        self
    }

    /// Sets the `package.readme` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .readme("README.adoc")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"readme = "README.adoc""#));
    /// /*
    /// [package]
    /// readme = "README.adoc"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn readme(&mut self, readme: &str) -> &mut Self {
        self.readme = Some(readme.into());
        self
    }

    /// Adds a keyword to the `package.keywords` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .keyword("async")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"keywords = ["async"]"#));
    /// /*
    /// [package]
    /// keywords = ["async"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn keyword(&mut self, keyword: &str) -> &mut Self {
        if let Some(ref mut v) = self.keywords {
            v.push(keyword.into());
        } else {
            self.keywords = Some(vec![keyword.into()]);
        }
        self
    }

    /// Sets the list of keywords for the `package.keywords` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .keywords(&[
    ///                         "async".into(),
    ///                         "networking".into(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"keywords = ["async", "networking"]"#));
    /// /*
    /// [package]
    /// keywords = ["async", "networking"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn keywords(&mut self, keywords: &[String]) -> &mut Self {
        self.keywords = Some(keywords.to_vec());
        self
    }

    /// Adds a category to the `package.categories` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .category("filesystem")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"categories = ["filesystem"]"#));
    /// /*
    /// [package]
    /// categories = ["filesystem"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn category(&mut self, category: &str) -> &mut Self {
        if let Some(ref mut v) = self.categories {
            v.push(category.into());
        } else {
            self.categories = Some(vec![category.into()]);
        }
        self
    }

    /// Sets the list of categories for the `package.categories` property
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .categories(&[
    ///                         "filesystem".into(),
    ///                         "testing".into(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"categories = ["filesystem", "testing"]"#));
    /// /*
    /// [package]
    /// categories = ["filesystem", "testing"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn categories(&mut self, categories: &[String]) -> &mut Self {
        self.categories = Some(categories.to_vec());
        self
    }

    /// Adds a license to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .license(License::Mit)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"license = "MIT""#));
    /// /*
    /// [package]
    /// license = "MIT"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn license<L: Into<License>>(&mut self, license: L) -> &mut Self {
        if let Some(ref mut v) = self.licenses {
            v.push(license.into());
        } else {
            self.licenses = Some(vec![license.into()]);
        }
        self
    }

    /// Sets a list of licenses for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .licenses(&[
    ///                         License::Mit,
    ///                         License::Apache2,
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"license = "MIT/Apache-2.0""#));
    /// /*
    /// [package]
    /// license = "MIT/Apache-2.0"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn licenses<L: Into<License> + Clone>(&mut self, licenses: &[L]) -> &mut Self {
        self.licenses = Some(licenses.to_vec().into_iter().map(|l| l.into()).collect::<Vec<_>>());
        self
    }

    /// Sets the location for the license file of the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .license_file("LICENSE")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"license-file = "LICENSE""#));
    /// /*
    /// [package]
    /// license-file = "LICENSE"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn license_file(&mut self, license_file: &str) -> &mut Self {
        self.license_file = Some(license_file.into());
        self
    }

    /// Sets the edition of the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .edition("2018")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"edition = "2018""#));
    /// /*
    /// [package]
    /// edition = "2018"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn edition(&mut self, edition: &str) -> &mut Self {
        self.edition = Some(edition.into());
        self
    }

    /// Sets the `badges.appveyor` table for the project
    ///
    /// You can import the "Appveyor" struct from `cargo_toml_builder::types`, but this method will
    /// take anything that implements `Into<Appveyor>`, which `String` and `&str` do. So if all you
    /// want is to add the repo, you can just pass some kind of string to this method and it should
    /// Just Work.
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Appveyor;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let appveyor_badge = Appveyor::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .appveyor(appveyor_badge)
    /// // or this will also work, and result in the same output:
    /// //                  .appveyor("https://gitlab.com/me/my-project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"appveyor = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// appveyor = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn appveyor<A: Into<Appveyor>>(&mut self, appveyor: A) -> &mut Self {
        self.appveyor = Some(appveyor.into());
        self
    }

    /// Sets the `badges.circle-ci` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::CircleCi;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let circe_ci_badge = CircleCi::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .circle_ci(circe_ci_badge)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"circle-ci = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// circle-ci = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn circle_ci<C: Into<CircleCi>>(&mut self, circle_ci: C) -> &mut Self {
        self.circle_ci = Some(circle_ci.into());
        self
    }

    /// Sets the `badges.gitlab` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Gitlab;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let gitlab_badge = Gitlab::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .gitlab(gitlab_badge)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"gitlab = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// gitlab = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn gitlab<G: Into<Gitlab>>(&mut self, gitlab: G) -> &mut Self {
        self.gitlab = Some(gitlab.into());
        self
    }

    /// Sets the `badges.travis-ci` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::TravisCi;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let travis_ci_badge = TravisCi::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .travis_ci(travis_ci_badge)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"travis-ci = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// travis-ci = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn travis_ci<T: Into<TravisCi>>(&mut self, travis_ci: T) -> &mut Self {
        self.travis_ci = Some(travis_ci.into());
        self
    }

    /// Sets the `badges.codecov` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Codecov;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let codecov_badge = Codecov::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .codecov(codecov_badge)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"codecov = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// codecov = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn codecov<C: Into<Codecov>>(&mut self, codecov: C) -> &mut Self {
        self.codecov = Some(codecov.into());
        self
    }

    /// Sets the `badges.coveralls` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Coveralls;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let coveralls_badge = Coveralls::new("https://gitlab.com/me/my-project");
    /// let cargo_toml = CargoToml::new()
    ///                     .coveralls(coveralls_badge)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"coveralls = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// coveralls = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn coveralls<C: Into<Coveralls>>(&mut self, coveralls: C) -> &mut Self {
        self.coveralls = Some(coveralls.into());
        self
    }

    /// Sets the `badges.is-it-maintained-issue-resolution` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Coveralls;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .is_it_maintained_time("https://gitlab.com/me/my-project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"is-it-maintained-issue-resolution = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// is-it-maintained-issue-resolution = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn is_it_maintained_time(&mut self, time: &str) -> &mut Self {
        self.is_it_maintained_time = Some(time.into());
        self
    }

    /// Sets the `badges.is-it-maintained-open-issues` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Coveralls;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .is_it_maintained_issues("https://gitlab.com/me/my-project")
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"is-it-maintained-open-issues = {repository = "https://gitlab.com/me/my-project"}"#));
    /// /*
    /// [badges]
    /// is-it-maintained-open-issues = { repository = "https://gitlab.com/me/my-project" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn is_it_maintained_issues(&mut self, issues: &str) -> &mut Self {
        self.is_it_maintained_issues = Some(issues.into());
        self
    }

    /// Sets the `badges.maintenance` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::MaintenanceStatus;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .maintenance(MaintenanceStatus::AsIs)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"maintenance = {status = "as-is"}"#));
    /// /*
    /// [badges]
    /// maintenance = { status = "as-is" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn maintenance<M: Into<MaintenanceStatus>>(&mut self, maintenance: M) -> &mut Self {
        self.maintenance = Some(Maintenance::from(maintenance.into()));
        self
    }

    /// Adds a dependency to the `dependencies` table
    ///
    /// The `Dependency` struct is imported through the prelude, but you can also pass anything to
    /// this that implements `Into<Dependency>` which `String` and `&str` do. If you pass a string
    /// to this method, it will add a regular crate dependency, with version `"*"`. You can use the
    /// extension methods from [`DependencyExt`](./types/trait.DependencyExt.html) to use a string to
    /// construct a `Dependency` object.
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .dependency("env_logger".version("0.5.6"))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"[dependencies]"#));
    /// # assert!(cargo_toml.to_string().contains(r#"env_logger = "0.5.6""#));
    /// /*
    /// [dependencies]
    /// env_logger = "0.5.6"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn dependency<D: Into<Dependency>>(&mut self, dependency: D) -> &mut Self {
        if let Some(ref mut deps) = self.dependencies {
            deps.push(dependency.into());
        } else {
            self.dependencies = Some(vec![dependency.into()]);
        }
        self
    }

    /// Set the `dependencies` table for the project
    ///
    /// **Note**: This will _replace_ any dependencies current attached to the object
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .dependencies(&[
    ///                         "env_logger".version("0.5.6"),
    ///                         "tokio".version("0.2"),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"env_logger = "0.5.6""#));
    /// # assert!(rendered.contains(r#"tokio = "0.2""#));
    /// /*
    /// [dependencies]
    /// env_logger = "0.5.6"
    /// tokio = "0.2"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn dependencies<D: Into<Dependency> + Clone>(&mut self, dependencies: &[D]) -> &mut Self {
        self.dependencies = Some(dependencies.into_iter().cloned().map(|d| d.into()).collect());
        self
    }

    /// Add a `dev-dependencies` entry to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .dev_dependency("hamcrest".version("0.1.5"))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[dev-dependencies]"#));
    /// # assert!(rendered.contains(r#"hamcrest = "0.1.5""#));
    /// /*
    /// [dev-dependencies]
    /// hamcrest = "0.1.5"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn dev_dependency<D: Into<Dependency>>(&mut self, dependency: D) -> &mut Self {
        if let Some(ref mut deps) = self.dev_dependencies {
            deps.push(dependency.into());
        } else {
            self.dev_dependencies = Some(vec![dependency.into()]);
        }
        self
    }

    /// Set the list of `dev-dependencies` for the project
    ///
    /// **Note**: This will _replace_ any dev-dependencies currently attached to the object
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .dev_dependencies(&[
    ///                         "hamcrest".version("0.1.5"),
    ///                         "my-test-lib".path("../my-test-lib")
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[dev-dependencies]"#));
    /// # assert!(rendered.contains(r#"hamcrest = "0.1.5""#));
    /// # assert!(rendered.contains(r#"my-test-lib = {path = "../my-test-lib"}"#));
    /// /*
    /// [dev-dependencies]
    /// hamcrest = "0.1.5"
    /// my-test-lib = { path = "../my-test-lib" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn dev_dependencies<D: Into<Dependency> + Clone>(&mut self, dependencies: &[D]) -> &mut Self {
        self.dev_dependencies = Some(dependencies.into_iter().cloned().map(|d| d.into()).collect());
        self
    }

    /// Add an entry to the `build-dependencies` table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .build_dependency("gcc".version("0.3.54"))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"[build-dependencies]"#));
    /// # assert!(cargo_toml.to_string().contains(r#"gcc = "0.3.54""#));
    /// /*
    /// [build-dependencies]
    /// gcc = "0.3.54"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn build_dependency<D: Into<Dependency>>(&mut self, dependency: D) -> &mut Self {
        if let Some(ref mut deps) = self.build_dependencies {
            deps.push(dependency.into());
        } else {
            self.build_dependencies = Some(vec![dependency.into()]);
        }
        self
    }

    /// Set the list of `build-dependencies` for the project
    ///
    /// **Note**: this will _replace_ any build dependencies currently attached to the object
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .build_dependencies(&[
    ///                         "gcc".version("0.3.54"),
    ///                         "cmake".version("0.1.29"),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[build-dependencies]"#));
    /// # assert!(rendered.contains(r#"gcc = "0.3.54""#));
    /// # assert!(rendered.contains(r#"cmake = "0.1.29""#));
    /// /*
    /// [build-dependencies]
    /// gcc = "0.3.54"
    /// cmake = "0.1.29"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn build_dependencies<D: Into<Dependency> + Clone>(&mut self, dependencies: &[D]) -> &mut Self {
        self.build_dependencies = Some(dependencies.into_iter().cloned().map(|d| d.into()).collect());
        self
    }

    /// Add a dependency to a target
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .target_dependency("cfg(unix)", "openssl".version("1.0.1"))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[target."cfg(unix)".dependencies]"#));
    /// # assert!(rendered.contains(r#"openssl = "1.0.1""#));
    /// /*
    /// [target."cfg(unix)".dependencies]
    /// openssl = "1.0.1"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn target_dependency<D: Into<Dependency>>(&mut self, name: &str, dependency: D) -> &mut Self {
        if let Some(ref mut map) = self.target_dependencies {
            let deps = map.entry(name.into()).or_insert(vec![]);
            deps.push(dependency.into());
        } else {
            let mut map = HashMap::new();
            map.insert(name.into(), vec![dependency.into()]);
            self.target_dependencies = Some(map);
        }
        self
    }

    /// Set the dependency list for a target
    ///
    /// **Note**: This will _replace_ any dependencies current attached to the target
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .target_dependencies("cfg(unix)", &[
    ///                         "openssl".version("1.0.1"),
    ///                         "libc".version("0.2.40"),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[target."cfg(unix)".dependencies]"#));
    /// # assert!(rendered.contains(r#"openssl = "1.0.1""#));
    /// # assert!(rendered.contains(r#"libc = "0.2.40""#));
    /// /*
    /// [target."cfg(unix)".dependencies]
    /// openssl = "1.0.1"
    /// libc = "0.2.40"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn target_dependencies<D: Into<Dependency> + Clone>(&mut self, name: &str, dependencies: &[D]) -> &mut Self {
        let deps = dependencies.into_iter().cloned().map(|d| d.into()).collect();
        if let Some(ref mut map) = self.target_dependencies {
            map.insert(name.into(), deps);
        } else {
            let mut map = HashMap::new();
            map.insert(name.into(), deps);
            self.target_dependencies = Some(map);
        }
        self
    }

    /// Set the properties of the dev profile
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::{Profile, PanicStrategy};
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let profile = Profile::new().panic(PanicStrategy::Abort).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .profile_dev(profile)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # assert!(cargo_toml.to_string().contains(r#"[profile.dev]"#));
    /// # assert!(cargo_toml.to_string().contains(r#"panic = "abort""#));
    /// /*
    /// [profile.dev]
    /// panic = "abort"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn profile_dev(&mut self, profile_dev: Profile) -> &mut Self {
        self.profile_dev = Some(profile_dev);
        self
    }

    /// Set the properties of the release profile
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::{Profile, PanicStrategy};
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let profile = Profile::new().panic(PanicStrategy::Abort).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .profile_release(profile)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[profile.release]"#));
    /// # assert!(rendered.contains(r#"panic = "abort""#));
    /// /*
    /// [profile.release]
    /// panic = "abort"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn profile_release(&mut self, profile_dev: Profile) -> &mut Self {
        self.profile_release = Some(profile_dev);
        self
    }

    /// Set the properties of the test profile
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::{Profile, PanicStrategy};
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let profile = Profile::new().panic(PanicStrategy::Abort).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .profile_test(profile)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[profile.test]"#));
    /// # assert!(rendered.contains(r#"panic = "abort""#));
    /// /*
    /// [profile.test]
    /// panic = "abort"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn profile_test(&mut self, profile_dev: Profile) -> &mut Self {
        self.profile_test = Some(profile_dev);
        self
    }

    /// Set the properties of the bench profile
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::{Profile, PanicStrategy};
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let profile = Profile::new().panic(PanicStrategy::Abort).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .profile_bench(profile)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[profile.bench]"));
    /// # assert!(rendered.contains(r#"panic = "abort""#));
    /// /*
    /// [profile.bench]
    /// panic = "abort"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn profile_bench(&mut self, profile_dev: Profile) -> &mut Self {
        self.profile_bench = Some(profile_dev);
        self
    }

    /// Set the properties of the doc profile
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::{Profile, PanicStrategy};
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let profile = Profile::new().panic(PanicStrategy::Abort).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .profile_doc(profile)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[profile.doc]"));
    /// # assert!(rendered.contains(r#"panic = "abort""#));
    /// /*
    /// [profile.doc]
    /// panic = "abort"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn profile_doc(&mut self, profile_dev: Profile) -> &mut Self {
        self.profile_doc = Some(profile_dev);
        self
    }

    /// Add a feature to the project
    ///
    /// The user can attach dependencies, and the labels for other features, to a `Feature` object.
    /// If a Dependency is added, it will automatically be added to the `[dependencies]` section of
    /// the document with the `optional = true` option added. If you need different behavior, add
    /// the name of the dependency using the `.feature` builder method and add the dependency to
    /// the correct section manually.
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let nightly = Feature::new("nightly").dependency("clippy".version("0.0.191")).build();
    /// let cargo_toml = CargoToml::new()
    ///                     .feature(nightly)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[features]"#));
    /// # assert!(rendered.contains(r#"nightly = ["clippy"]"#));
    /// # assert!(rendered.contains(r#"clippy = {version = "0.0.191",optional = true}"#));
    /// /*
    /// [features]
    /// nightly = ['clippy']
    ///
    /// [dependencies]
    /// clippy = { version = "0.0.191", optional = true }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    ///
    /// Or, to add a feature dependency to, say, `[dev-dependencies]`:
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let nightly = Feature::new("nightly").feature("clippy").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .feature(nightly)
    ///                     .dev_dependency("clippy".version("0.0.191").optional(true))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains(r#"[features]"#));
    /// # assert!(rendered.contains(r#"nightly = ["clippy"]"#));
    /// # assert!(rendered.contains(r#"[dev-dependencies]"#));
    /// # assert!(rendered.contains(r#"clippy = {version = "0.0.191",optional = true}"#));
    /// /*
    /// [features]
    /// nightly = ['clippy']
    ///
    /// [dev-dependencies]
    /// clippy = { version = "0.0.191", optional = true }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn feature<F: Into<Feature>>(&mut self, feature: F) -> &mut Self {
        if let Some(ref mut v) = self.features {
            v.push(feature.into());
        } else {
            self.features = Some(vec![feature.into()]);
        }
        self
    }

    /// Set the workspace config table for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::Workspace;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let workspace = Workspace::new().member("/path/to/member1").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .workspace(workspace)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[workspace]"));
    /// # assert!(rendered.contains(r#"members = ["/path/to/member1"]"#));
    /// /*
    /// [workspace]
    /// members = ["/path/to/member1"]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn workspace(&mut self, workspace: Workspace) -> &mut Self {
        self.workspace = Some(workspace);
        self
    }

    /// Set a `[lib]` target for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let target = LibTarget::new().name("my-lib").path("src/my-lib.rs").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .lib(target)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[lib]"));
    /// # assert!(rendered.contains(r#"name = "my-lib""#));
    /// # assert!(rendered.contains(r#"path = "src/my-lib.rs""#));
    /// /*
    /// [lib]
    /// name = "my-lib"
    /// path = "src/my-lib.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn lib<T: Into<LibTarget>>(&mut self, target: T) -> &mut Self {
        self.lib = Some(target.into());
        self
    }

    /// Add a `[[bin]]` target for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let target = BinTarget::new().name("my-bin").path("src/bin/my-bin.rs").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .bin(target)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[bin]]"));
    /// # assert!(rendered.contains(r#"name = "my-bin""#));
    /// # assert!(rendered.contains(r#"path = "src/bin/my-bin.rs""#));
    /// /*
    /// [dependencies]
    /// <name> = <value>
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn bin<T: Into<BinTarget>>(&mut self, target: T) -> &mut Self {
        if let Some(ref mut v) = self.bins {
            v.push(target.into());
        } else {
            self.bins = Some(vec![target.into()]);
        }
        self
    }

    /// Set the `[[bin]]` targets for the project
    ///
    /// **Note**: This will _replace_ any bin targets currently attached to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .bins(&[
    ///                         BinTarget::new().name("my-bin-1").path("src/bin/my-bin-1.rs").build(),
    ///                         BinTarget::new().name("my-bin-2").path("src/bin/my-bin-2.rs").build(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[bin]]"));
    /// # assert!(rendered.contains(r#"name = "my-bin-1""#));
    /// # assert!(rendered.contains(r#"name = "my-bin-2""#));
    /// # assert!(rendered.contains(r#"path = "src/bin/my-bin-1.rs""#));
    /// # assert!(rendered.contains(r#"path = "src/bin/my-bin-2.rs""#));
    /// /*
    /// [dependencies]
    /// <name> = <value>
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn bins(&mut self, target: &[BinTarget]) -> &mut Self {
        self.bins = Some(target.to_vec());
        self
    }

    /// Add a `[bench]` target for the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let target = BenchTarget::new().name("my-benchmark").path("bench/my-benchmark.rs").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .bench(target)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[bench]]"));
    /// # assert!(rendered.contains(r#"name = "my-benchmark""#));
    /// # assert!(rendered.contains(r#"path = "bench/my-benchmark.rs""#));
    /// /*
    /// [dependencies]
    /// <name> = <value>
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn bench<T: Into<BenchTarget>>(&mut self, target: T) -> &mut Self {
        if let Some(ref mut v) = self.benches {
            v.push(target.into());
        } else {
            self.benches = Some(vec![target.into()]);
        }
        self
    }

    /// Set the list of `[[bench]]` targets for the project
    ///
    /// **Note**: This will _replace_ any bench targets currently attached to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .benches(&[
    ///                         BenchTarget::new().name("my-benchmark-1").path("bench/my-benchmark-1.rs").build(),
    ///                         BenchTarget::new().name("my-benchmark-2").path("bench/my-benchmark-2.rs").build(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    ///
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[bench]]"));
    /// # assert!(rendered.contains(r#"name = "my-benchmark-1""#));
    /// # assert!(rendered.contains(r#"name = "my-benchmark-2""#));
    /// # assert!(rendered.contains(r#"path = "bench/my-benchmark-1.rs""#));
    /// # assert!(rendered.contains(r#"path = "bench/my-benchmark-2.rs""#));
    /// /*
    /// [[bench]]
    /// name = "my-benchmark-1"
    /// path = "bench/my-benchmark-1.rs"
    ///
    /// [[bench]]
    /// name = "my-benchmark-2"
    /// path = "bench/my-benchmark-2.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn benches(&mut self, target: &[BenchTarget]) -> &mut Self {
        self.benches = Some(target.to_vec());
        self
    }

    /// Add a `[[test]]` target to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let target = TestTarget::new().name("my-test").path("tests/my-test.rs").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .test(target)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[test]]"));
    /// # assert!(rendered.contains(r#"name = "my-test""#));
    /// # assert!(rendered.contains(r#"path = "tests/my-test.rs""#));
    /// /*
    /// [[test]]
    /// name = "my-test"
    /// path = "tests/my-test.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn test<T: Into<TestTarget>>(&mut self, target: T) -> &mut Self {
        if let Some(ref mut v) = self.tests {
            v.push(target.into());
        } else {
            self.tests = Some(vec![target.into()]);
        }
        self
    }

    /// Set the `[[test]]` targets for the project
    ///
    /// **Note**: This will _replace_ any test targets currently attached to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .tests(&[
    ///                         TestTarget::new().name("my-test-1").path("tests/my-test-1.rs").build(),
    ///                         TestTarget::new().name("my-test-2").path("tests/my-test-2.rs").build(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[test]]"));
    /// # assert!(rendered.contains(r#"name = "my-test-1""#));
    /// # assert!(rendered.contains(r#"name = "my-test-2""#));
    /// # assert!(rendered.contains(r#"path = "tests/my-test-1.rs""#));
    /// # assert!(rendered.contains(r#"path = "tests/my-test-2.rs""#));
    /// /*
    /// [[test]]
    /// name = "my-test-1"
    /// path = "tests/my-test-1.rs"
    ///
    /// [[test]]
    /// name = "my-test-2"
    /// path = "tests/my-test-2.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn tests(&mut self, target: &[TestTarget]) -> &mut Self {
        self.tests = Some(target.to_vec());
        self
    }

    /// Add an `[[example]]` target to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let target = ExampleTarget::new().name("my-example").path("examples/my-example.rs").build();
    /// let cargo_toml = CargoToml::new()
    ///                     .example(target)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[example]]"));
    /// # assert!(rendered.contains(r#"name = "my-example""#));
    /// # assert!(rendered.contains(r#"path = "examples/my-example.rs""#));
    /// /*
    /// [[example]]
    /// name = "my-example"
    /// path = "examples/my-example.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn example<T: Into<ExampleTarget>>(&mut self, target: T) -> &mut Self {
        if let Some(ref mut v) = self.examples {
            v.push(target.into());
        } else {
            self.examples = Some(vec![target.into()]);
        }
        self
    }

    /// Set the list of `[[example]]` targets for the project
    //
    /// **Note**: This will _replace_ any `[[example]]` targets currently attached to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .examples(&[
    ///                         ExampleTarget::new().name("my-example-1").path("examples/my-example-1.rs").build(),
    ///                         ExampleTarget::new().name("my-example-2").path("examples/my-example-2.rs").build(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[[example]]"));
    /// # assert!(rendered.contains(r#"name = "my-example-1""#));
    /// # assert!(rendered.contains(r#"name = "my-example-2""#));
    /// # assert!(rendered.contains(r#"path = "examples/my-example-1.rs""#));
    /// # assert!(rendered.contains(r#"path = "examples/my-example-2.rs""#));
    /// /*
    /// [[example]]
    /// name = "my-example-1"
    /// path = "examples/my-example-1.rs"
    ///
    /// [[example]]
    /// name = "my-example-2"
    /// path = "examples/my-example-2.rs"
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn examples(&mut self, target: &[ExampleTarget]) -> &mut Self {
        self.examples = Some(target.to_vec());
        self
    }

    /// <DESCRIPTION>
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::PatchSet;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let patchset = PatchSet::new("crates-io")
    ///                         .patch("env_logger".repo("https://gitlab.com/me/env_logger.git"))
    ///                         .build();
    /// let cargo_toml = CargoToml::new()
    ///                     .patch(patchset)
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[patch.crates-io]"));
    /// # assert!(rendered.contains(r#"env_logger = {git = "https://gitlab.com/me/env_logger.git"}"#));
    /// /*
    /// [patch.crates-io]
    /// env_logger = { git = "https://gitlab.com/me/env_logger.git" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn patch(&mut self, patch: PatchSet) -> &mut Self {
        if let Some(ref mut v) = self.patches {
            v.push(patch);
        } else {
            self.patches = Some(vec![patch]);
        }
        self
    }

    /// Set the list of patch sets for the project
    ///
    /// **Note**: This will _replace_ any patch sets currently attached to the object
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// use cargo_toml_builder::types::PatchSet;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .patches(&[
    ///                         PatchSet::new("crates-io")
    ///                                  .patch("env_logger".repo("https://gitlab.com/me/env_logger.git"))
    ///                                  .build(),
    ///                         PatchSet::new("https://github.com/example/baz")
    ///                                  .patch("baz".repo("https://github.com/example/patched-baz"))
    ///                                  .build(),
    ///                     ])
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[patch.crates-io]"));
    /// # assert!(rendered.contains(r#"[patch."https://github.com/example/baz"]"#));
    /// # assert!(rendered.contains(r#"env_logger = {git = "https://gitlab.com/me/env_logger.git"}"#));
    /// # assert!(rendered.contains(r#"baz = {git = "https://github.com/example/patched-baz"}"#));
    /// /*
    /// [patch.crates-io]
    /// env_logger = { git = "https://gitlab.com/me/env_logger.git" }
    ///
    /// [patch."https://github.com/example/baz"]
    /// baz = { git = "https://github.com/example/patched-baz" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn patches(&mut self, patches: &[PatchSet]) -> &mut Self {
        self.patches = Some(patches.to_vec());
        self
    }

    /// Add a `[replace]` entry to the project
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    ///
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let cargo_toml = CargoToml::new()
    ///                     .replace("foo:0.1.0".repo("https://github.com/example/foo"))
    /// #                   .author("me")
    /// #                   .name("my-project")
    ///                     .build()?;
    /// # let rendered = cargo_toml.to_string();
    /// # assert!(rendered.contains("[replace]"));
    /// # assert!(rendered.contains(r#""foo:0.1.0" = {git = "https://github.com/example/foo"}"#));
    /// /*
    /// [replace]
    /// "foo:0.1.0" = { git = "https://github.com/example/foo" }
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn replace<D: Into<Dependency>>(&mut self, dependency: D) -> &mut Self {
        let replace = Replace::new(dependency.into());
        if let Some(ref mut v) = self.replacements {
            v.push(replace);
        } else {
            self.replacements = Some(vec![replace]);
        }
        self
    }

    /// Build a `toml_edit::Document` object from this builder
    ///
    /// # Example
    ///
    /// ```rust
    /// extern crate cargo_toml_builder;
    /// use cargo_toml_builder::prelude::*;
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    ///
    /// let cargo_toml = CargoToml::new()
    ///                     .name("my-project")
    ///                     .author("Me <me@me.com>")
    ///                     .build()?;
    /// /*
    /// [package]
    /// name = "my-project"
    /// version = "0.1.0"
    /// authors = ["Me <me@me.com>"]
    ///
    /// [dependencies]
    /// */
    /// #   Ok(())
    /// # }
    /// ```
    pub fn build(&self) -> Result<Document, Error> {
        Ok(TomlOutput::new(self).document()?)
    }
}

