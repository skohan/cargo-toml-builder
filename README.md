# Cargo.toml Builder

Programmatically generate Cargo.toml files

## Installation

Add the following to your `Cargo.toml` file:

```toml
[dependencies]
cargo-toml-builder = "0.3"
```

and the following to your crate root:

```rust
extern crate cargo_toml_builder;
```

## Example

```rust
extern crate cargo_toml_builder;

use cargo_toml_builder::prelude::*;

let cargo_toml = CargoToml::builder()
                      .name("my-project")
                      .version("1.0.0")
                      .author("Alice Smith <asmith@example.com>")
                      .dependency("env_logger".version("0.5.6"))
                      .feature(Feature::new("nightly").dependency("clippy"))
                      .build()?;

assert_eq!(cargo_toml.to_string(), r#"
[package]
name = "my-project"
version = "1.0.0"
authors = ["Alice Smith <asmith@example.com>"]

[dependencies]
env_logger = "0.5.6"
clippy = {"version" = "*", optional = true}

[features]
nightly = ["clippy"]
"#);
```

